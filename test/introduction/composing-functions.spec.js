const { expect } = require('chai')

const { doStuff, doStuffBetter, doStuffObservable, doStuffBetterObservable, g, f, wait } = require('../../src/introduction/composing-functions.js')

describe('Composing Software: An Introduction', () => {
  describe('Composing functions', () => {
    it('When doStuff of 20 should return 42', () => {
      expect(doStuff(20)).to.be.equal(42)
    })
    it('When doStuffObservable of 20 should return 42 and output log', () => {
      expect(doStuffObservable(20)).to.be.equal(42)
    })
    it('When doStuffBetter of 20 should return 42', () => {
      expect(doStuffBetter(20)).to.be.equal(42)
    })
    it('When doStuffBetterObservable of 20 should return 42', () => {
      expect(doStuffBetterObservable(20)).to.be.equal(42)
    })
    it('Given chain promises, when wait and chain should return 42', () => {
      return wait(300)
        .then(() => 20)
        .then(g)
        .then(f)
        .then(result => expect(result).to.be.equal(42))
    })
  })
})
