const { expect } = require('chai')
const { Bar, c } = require('./../../src/introduction/composing-objects.js')

describe('Composing Software: An Introduction', () => {
  describe('Composing objects', () => {
    it('When composing with class inheritance should have all elements', () => {
      const myBar = new Bar()
      expect(myBar.b).to.not.be.equal(undefined)
    })
    it('When composing objects with mixin b is not defined as well', () => {
      expect(c.b).to.be.equal('b')
    })
  })
})
