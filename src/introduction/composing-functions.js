const trace = require('./trace.js')
const pipe = require('./pipe.js')

const g = n => n + 1
const f = n => n * 2

const doStuff = x => {
  const afterG = g(x)
  const afterF = f(afterG)
  return afterF
}

const doStuffObservable = x => {
  const afterG = g(x)
  console.log(`after g: ${afterG}`)
  const afterF = f(afterG)
  console.log(`after f: ${afterF}`)
  return afterF
}

const doStuffBetter = x => f(g(x))
const doStuffBetterObservable = pipe(
  g,
  trace('after g'),
  f,
  trace('after f')
)

const wait = time => new Promise((resolve, reject) => setTimeout(resolve, time))

module.exports = {
  doStuff,
  doStuffBetter,
  doStuffObservable,
  doStuffBetterObservable,
  g,
  f,
  wait
}
